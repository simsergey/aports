# Contributor: Paul Bredbury <brebs@sent.com>
# Maintainer: gay <gay@disroot.org>
pkgname=icewm
pkgver=3.1.0
pkgrel=0
pkgdesc="Window manager designed for speed, usability and consistency"
url="https://github.com/ice-wm/icewm"
arch="all"
options="!check" # No test suite
license="LGPL-2.0-only"
subpackages="$pkgname-doc $pkgname-lang"
makedepends="
	alsa-lib-dev
	cmake
	fribidi-dev
	glib-dev
	imlib2-dev
	libao-dev
	libintl
	librsvg-dev
	libsm-dev
	libsndfile-dev
	libxcomposite-dev
	libxdamage-dev
	libxft-dev
	libxinerama-dev
	libxpm-dev
	libxrandr-dev
	markdown
	perl
	samurai
	"
source="https://github.com/ice-wm/icewm/releases/download/$pkgver/icewm-$pkgver.tar.lz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCFGDIR=/etc/icewm \
		-DENABLE_NLS=OFF \
		-DCONFIG_IMLIB2=ON \
		-DCONFIG_LIBRSVG=ON \
		-DENABLE_LTO=ON \
		-DDOCDIR=/usr/share/doc/icewm
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
40c074c084ebc627bdab2bdaffe1cfbca2752e054abaa99de2078477158122f21d47fe9a0d219419b806da33b7e5b8c267d38b4a51529e5340ab70d9eb4eca44  icewm-3.1.0.tar.lz
"
